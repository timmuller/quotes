from unittest import mock

from django.test import TestCase as DjangoTestCase


class TestCase(DjangoTestCase):
    def setup_patch(self, target):
        mockclass = mock.patch(target)
        mock_object = mockclass.start()
        self.addCleanup(mock_object.stop)
        return mock_object
