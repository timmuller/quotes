import factory
from factory.django import DjangoModelFactory

from quotes_api.models import QuoteRating


class QuoteRatingFactory(DjangoModelFactory):
    class Meta:
        model = QuoteRating

    quote_id = 1
    username = factory.Sequence(lambda x: "Username{}".format(x))
    rating = 1
