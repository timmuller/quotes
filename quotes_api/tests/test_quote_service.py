from json import JSONDecodeError

from requests import Response, HTTPError

from quotes_api.quote_service import get_random_quote, get_quote_by_id
from quotes_api.tests.testcase import TestCase


class StubbedResponse(Response):
    def __init__(self, content, status_code=200):
        super(StubbedResponse, self).__init__()
        self.status_code = status_code
        self._content = content


class QuoteServiceBase(TestCase):
    def setUp(self):
        self.stubbed_quote_service_response = StubbedResponse(
            b'{"author":"Donald Knuth","id":27,'
            b'"quote":"Beware of bugs in the above code; I have only proved it correct, not tried it. ",'
            b'"permalink":"http://quotes.stormconsultancy.co.uk/quotes/27"}'
        )
        self.mock_request_get = self.setup_patch('quotes_api.quote_service.requests.get')
        self.mock_request_get.return_value = self.stubbed_quote_service_response


class TestGetRandomQuote(QuoteServiceBase):
    def test_that_random_quote_url_is_called(self):
        get_random_quote()

        self.mock_request_get.assert_called_once_with(
            'http://quotes.stormconsultancy.co.uk/random.json'
        )

    def test_that_quote_is_returned(self):
        quote = get_random_quote()
        self.assertEqual(quote.id, 27)
        self.assertEqual(quote.author, "Donald Knuth")
        self.assertEqual(quote.quote, "Beware of bugs in the above code; I have only proved it correct, not tried it. ")
        self.assertEqual(quote.link, "http://quotes.stormconsultancy.co.uk/quotes/27")

    def test_that_error_of_service_is_raised(self):
        self.mock_request_get.return_value = StubbedResponse(status_code=500, content=b'error')
        with self.assertRaises(HTTPError):
            get_random_quote()

    def test_that_non_decoded_json_exception_is_raised_on_invalid_response_data(self):
        self.mock_request_get.return_value = StubbedResponse(status_code=200, content=b'invalid')
        with self.assertRaises(JSONDecodeError):
            get_random_quote()

    def test_that_missing_key_in_responses_raises_keyError(self):
        self.mock_request_get.return_value = StubbedResponse(status_code=200, content=b'{}')
        with self.assertRaises(KeyError):
            get_random_quote()


class TestGetQuoteByID(QuoteServiceBase):
    # def test_that_correct_url_is_called(self):
    #     get_quote_by_id(15)
    #
    #     self.mock_request_get.assert_called_once_with(
    #         'http://quotes.stormconsultancy.co.uk/quotes/15.json'
    #     )
    #
    # def test_that_quote_is_returned(self):
    #     quote = get_quote_by_id(27)
    #     self.assertEqual(quote.id, 27)
    #     self.assertEqual(quote.author, "Donald Knuth")
    #     self.assertEqual(quote.quote, "Beware of bugs in the above code; I have only proved it correct, not tried it. ")
    #     self.assertEqual(quote.link, "http://quotes.stormconsultancy.co.uk/quotes/27")
    #
    # def test_that_error_of_service_is_raised(self):
    #     self.mock_request_get.return_value = StubbedResponse(status_code=500, content=b'error')
    #     with self.assertRaises(HTTPError):
    #         get_quote_by_id(27)
    #
    # def test_that_non_decoded_json_exception_is_raised_on_invalid_response_data(self):
    #     self.mock_request_get.return_value = StubbedResponse(status_code=200, content=b'invalid')
    #     with self.assertRaises(JSONDecodeError):
    #         get_quote_by_id(27)
    #
    # def test_that_missing_key_in_responses_raises_keyError(self):
    #     self.mock_request_get.return_value = StubbedResponse(status_code=200, content=b'{}')
    #     with self.assertRaises(KeyError):
    #         get_quote_by_id(27)

    def test_that_response_with_special_characters_is_encoded_properly(self):
        self.mock_request_get.return_value = StubbedResponse(
            b'{"author":"Donald Knuth","id":27,'
            b'"quote":"\\\\xf0\\\\x9f\\\\x90\\\\xae",'
            b'"permalink":"http://quotes.stormconsultancy.co.uk/quotes/27"}'
        )
        quote = get_quote_by_id(27)
        self.assertEqual(quote.quote, "🐮")
