from json import JSONDecodeError

from django.urls import reverse
from requests import HTTPError

from quotes_api.quote_service import Quote
from quotes_api.tests.factories import QuoteRatingFactory
from quotes_api.tests.testcase import TestCase


class TestFetchQuote(TestCase):
    def setUp(self):
        self.url = reverse('quote', kwargs={'id': '1'})
        self.mock_get_quote_by_id = self.setup_patch('quotes_api.views.quote.get_quote_by_id')
        self.mock_get_quote_by_id.return_value = Quote(
            id=1,
            author="test",
            quote="testQuote",
            link='link'
        )

    def test_url(self):
        self.assertEqual(self.url, '/api/quote/1/')

    def test_that_200_is_returned_when_quote_exists(self):
        ret = self.client.get(self.url)
        self.assertEqual(ret.status_code, 200)

    def test_that_get_quote_by_id_is_called_correctly(self):
        self.client.get(self.url)
        self.mock_get_quote_by_id.assert_called_once_with(1)

    def test_that_quote_is_returned_by_id(self):
        ret = self.client.get(self.url)
        self.assertEqual(ret.json(), {
            'id': 1,
            'author': 'test',
            'quote': 'testQuote',
            'link': 'link',
            'average_rating': None,
            'number_of_rates': 0,
        })

    def test_that_404_is_returned_when_no_quote_found(self):
        self.mock_get_quote_by_id.return_value = None
        ret = self.client.get(self.url)

        self.assertEqual(ret.status_code, 404)

    def test_that_unavailable_is_returned_when_API_gives_error_response_code(self):
        self.mock_get_quote_by_id.side_effect = HTTPError
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 503)
        self.assertEqual(response.json(), {
            'message': 'Quotes API is unavailable'
        })

    def test_that_non_json_data_result_in_error_message(self):
        self.mock_get_quote_by_id.side_effect = JSONDecodeError('e', 'e', 1)
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json(), {
            'message': 'Unexpected quotation data received'
        })

    def test_that_missing_data_key_within_data_result_in_error_message(self):
        self.mock_get_quote_by_id.side_effect = KeyError
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json(), {
            'message': 'Unexpected quotation data received'
        })

    def test_that_summary_of_one_rating_is_returned(self):
        QuoteRatingFactory.create(quote_id=1, rating=5)

        ret = self.client.get(self.url)
        self.assertEqual(ret.json(), {
            'id': 1,
            'author': 'test',
            'quote': 'testQuote',
            'link': 'link',
            'average_rating': '5.0',
            'number_of_rates': 1,
        })

    def test_that_summary_of_multiple_ratings_is_returned(self):
        QuoteRatingFactory.create(quote_id=1, rating=5)
        QuoteRatingFactory.create(quote_id=1, rating=4)
        QuoteRatingFactory.create(quote_id=1, rating=4)

        ret = self.client.get(self.url)
        self.assertEqual(ret.json(), {
            'id': 1,
            'author': 'test',
            'quote': 'testQuote',
            'link': 'link',
            'average_rating': '4.33',
            'number_of_rates': 3,
        })
