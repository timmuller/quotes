from json import JSONDecodeError
from django.urls import reverse
from requests import HTTPError

from quotes_api.quote_service import Quote
from quotes_api.tests.factories import QuoteRatingFactory
from quotes_api.tests.testcase import TestCase


class TestFetchRandomQuote(TestCase):
    def setUp(self):
        self.url = reverse('random_quote')
        self.mock_get_random_quote = self.setup_patch('quotes_api.views.random_quote.get_random_quote')
        self.mock_get_random_quote.return_value = Quote(
            id=27,
            author="Donald Knuth",
            quote="Beware of bugs in the above code; I have only proved it correct, not tried it. ",
            link="http://quotes.stormconsultancy.co.uk/quotes/27"
        )

    def test_that_random_quote_endpoint_is_available(self):
        self.assertEqual(self.url, '/api/random_quote/')

    def test_that_quote_data_is_returned_in_data_response_with_200(self):
        ret = self.client.get(self.url)

        self.assertEqual(ret.status_code, 200)
        self.assertEqual(ret.json(), {
            'id': 27,
            'author': 'Donald Knuth',
            'quote': 'Beware of bugs in the above code; I have only proved it correct, not tried it. ',
            'link': 'http://quotes.stormconsultancy.co.uk/quotes/27',
            'average_rating': None,
            'number_of_rates': 0,
        })

    def test_that_unavailable_is_returned_when_API_gives_error_response_code(self):
        self.mock_get_random_quote.side_effect = HTTPError
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 503)
        self.assertEqual(response.json(), {
            'message': 'Quotes API is unavailable'
        })

    def test_that_non_json_data_result_in_error_message(self):
        self.mock_get_random_quote.side_effect = JSONDecodeError('e', 'e', 1)
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json(), {
            'message': 'Unexpected quotation data received'
        })

    def test_that_missing_data_key_within_data_result_in_error_message(self):
        self.mock_get_random_quote.side_effect = KeyError
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.json(), {
            'message': 'Unexpected quotation data received'
        })

    def test_that_average_rating_is_returned_with_rating_count_one(self):
        QuoteRatingFactory.create(quote_id=27, rating=5)
        ret = self.client.get(self.url)
        data = ret.json()
        self.assertEqual(data['average_rating'], '5.0')
        self.assertEqual(data['number_of_rates'], 1)

    def test_that_average_rating_is_returned_with_rating_count_three(self):
        QuoteRatingFactory.create(quote_id=27, rating=5)
        QuoteRatingFactory.create(quote_id=27, rating=3)
        QuoteRatingFactory.create(quote_id=27, rating=3)

        ret = self.client.get(self.url)
        data = ret.json()
        self.assertEqual(data['average_rating'], '3.67')
        self.assertEqual(data['number_of_rates'], 3)
