import json

from django.test import TestCase
from django.urls import reverse

from quotes_api.models import QuoteRating
from quotes_api.tests.factories import QuoteRatingFactory


class TestStoreRating(TestCase):
    def setUp(self):
        self.url = reverse('quote_rating', kwargs={'quote': 1})

    def test_that_new_rating_is_returned(self):
        postdata = {
            'username': 'username',
            'rating': 5,
        }
        ret = self.client.post(self.url, data=json.dumps(postdata), content_type='json')
        self.assertEqual(ret.json(), [{
            'username': 'username',
            'rating': '5',
        }])

    def test_that_new_rating_with_previous_ratings_is_returned(self):
        QuoteRatingFactory.create(quote_id=1, rating=1, username='older')
        postdata = {
            'username': 'username',
            'rating': 5,
        }
        ret = self.client.post(self.url, data=json.dumps(postdata), content_type='json')
        self.assertEqual(ret.json(), [
            {
                'username': 'older',
                'rating': '1',
            },
            {
                'username': 'username',
                'rating': '5',
            },
        ])

    def test_that_rating_is_stored_by_username(self):
        postdata = {
            'username': 'username',
            'rating': 5,
        }
        ret = self.client.post(self.url, data=json.dumps(postdata), content_type='json')
        self.assertEqual(ret.status_code, 200)
        self.assertEqual(QuoteRating.objects.count(), 1)
        quote = QuoteRating.objects.first()
        self.assertEqual(quote.quote_id, 1)
        self.assertEqual(quote.username, 'username')
        self.assertEqual(quote.rating, 5)

    def test_that_rating_is_updated_when_username_already_rated_quote(self):
        rating = QuoteRatingFactory.create(
            quote_id=1,
            username="username",
            rating=0
        )
        postdata = {
            'username': 'username',
            'rating': 5,
        }
        self.client.post(self.url, data=json.dumps(postdata), content_type='json')

        rating.refresh_from_db()
        self.assertEqual(rating.rating, 5)

    def test_that_quotation_can_be_rated_mulitple_times_by_different_usernames(self):
        QuoteRatingFactory.create(quote_id=1, username='username')
        postdata = {
            'username': 'other_username',
            'rating': 5,
        }
        self.client.post(self.url, data=json.dumps(postdata), content_type='json')
        self.assertEqual(QuoteRating.objects.count(), 2)

    def test_that_400_is_returned_when_empty_payload_is_send(self):
        ret = self.client.post(self.url, content_type='json')
        self.assertEqual(ret.status_code, 400)
        self.assertEqual(ret.json(), {
            'username': ['This field is required.'],
            'rating': ['This field is required.'],
        })

    def test_that_400_is_returned_when_empty_payload_with_incorrect_rating(self):
        postdata = {
            'username': 'username',
            'rating': 'not_valid_rating'
        }
        ret = self.client.post(self.url, data=json.dumps(postdata), content_type='json')
        self.assertEqual(ret.json(), {
            'rating': ['Enter a number.'],
        })

    def test_that_rating_cannot_be_negative(self):
        postdata = {
            'username': 'username',
            'rating': '-1'
        }
        ret = self.client.post(self.url, data=json.dumps(postdata), content_type='json')
        self.assertEqual(ret.json(), {
            'rating': ['Ensure this value is greater than or equal to 0.'],
        })

    def test_that_rating_cannot_be_higher_than_5(self):
        postdata = {
            'username': 'username',
            'rating': '6'
        }
        ret = self.client.post(self.url, data=json.dumps(postdata), content_type='json')
        self.assertEqual(ret.json(), {
            'rating': ['Ensure this value is less than or equal to 5.'],
        })

    def test_that_username_cannot_be_empty(self):
        postdata = {
            'username': '',
            'rating': '0'
        }
        ret = self.client.post(self.url, data=json.dumps(postdata), content_type='json')
        self.assertEqual(ret.json(), {
            'username': ['This field is required.'],
        })
