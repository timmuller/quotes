from django.test import TestCase
from django.urls import reverse

from quotes_api.tests.factories import QuoteRatingFactory


class TestStoreRatings(TestCase):
    def setUp(self):
        self.url = reverse('quote_rating', kwargs={'quote': 1})

    def test_that_no_ratings_of_quote_is_returned_when_no_ratings_available(self):
        ret = self.client.get(self.url)
        self.assertEqual(ret.status_code, 200)
        self.assertEqual(ret.json(), [])

    def test_that_one_rating_is_returned(self):
        QuoteRatingFactory.create(
            quote_id=1,
            username='username',
            rating=5,
        )
        ret = self.client.get(self.url)
        self.assertEqual(ret.status_code, 200)
        self.assertEqual(ret.json(), [{
            'username': 'username',
            'rating': '5',
        }])

    def test_that_multiple_rating_are_returned(self):
        QuoteRatingFactory.create_batch(3, quote_id=1)

        ret = self.client.get(self.url)

        self.assertEqual(len(ret.json()), 3)

    def test_that_rating_of_other_quote_is_not_included(self):
        QuoteRatingFactory.create(
            quote_id=1,
            username='username',
            rating=5,
        )
        QuoteRatingFactory.create(quote_id=2, username='username')

        ret = self.client.get(self.url)

        self.assertEqual(len(ret.json()), 1)
