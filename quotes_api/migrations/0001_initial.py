# Generated by Django 3.1.4 on 2020-12-09 20:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='QuoteRating',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quote_id', models.IntegerField()),
                ('username', models.CharField(max_length=255)),
                ('rating', models.IntegerField()),
            ],
            options={
                'unique_together': {('quote_id', 'username')},
            },
        ),
    ]
