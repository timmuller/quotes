from django.db import models


class QuoteRating(models.Model):
    class Meta:
        unique_together = ('quote_id', 'username')

    quote_id = models.IntegerField()
    username = models.CharField(max_length=255)
    rating = models.IntegerField()
