from django.urls import path

from quotes_api.views.quote import QuoteDetailView
from quotes_api.views.random_quote import RandomQuoteView
from quotes_api.views.rating_view import RatingView

urlpatterns = [
    path('random_quote/', RandomQuoteView.as_view(), name='random_quote'),
    path('quote/<int:id>/', QuoteDetailView.as_view(), name='quote'),
    path('quote/<int:quote>/rate/', RatingView.as_view(), name='quote_rating')
]
