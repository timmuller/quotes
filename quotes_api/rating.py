from django.db.models import Avg, Count

from quotes_api.models import QuoteRating


def get_rate_information(quote_id):
    rating_information = QuoteRating.objects.filter(quote_id=quote_id).aggregate(
        average_rating=Avg('rating'),
        number_of_rates=Count('*'),
    )
    average_rating = rating_information['average_rating']

    if average_rating is not None:
        average_rating = round(average_rating, 2)

    return average_rating, rating_information['number_of_rates']
