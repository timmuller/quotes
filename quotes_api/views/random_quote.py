from json import JSONDecodeError

from django.http import JsonResponse
from django.views import View
from requests import HTTPError

from quotes_api.quote_service import get_random_quote
from quotes_api.rating import get_rate_information


class RandomQuoteView(View):
    def get(self, request, *args, **kwargs):
        try:
            random_quote = get_random_quote()
        except HTTPError:
            return JsonResponse({'message': 'Quotes API is unavailable'}, status=503)
        except (JSONDecodeError, KeyError):
            return JsonResponse({'message': 'Unexpected quotation data received'}, status=500)

        average_rating, number_of_rates = get_rate_information(random_quote.id)
        return JsonResponse({
            'average_rating': str(average_rating) if average_rating else None,
            'number_of_rates': number_of_rates,
            **random_quote._asdict()
        })
