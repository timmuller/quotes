import json
from json import JSONDecodeError

from django import forms
from django.http import JsonResponse, HttpResponse
from django.views import View
from quotes_api.models import QuoteRating


class RatingForm(forms.Form):
    username = forms.CharField()
    rating = forms.DecimalField(min_value=0, max_value=5)


class RatingView(View):
    def get(self, request, quote, *args, **kwargs):
        return self.get_json_response_for_quote_rating(quote)

    def post(self, request, quote, *args, **kwargs):
        try:
            form = RatingForm(json.loads(request.body))
        except JSONDecodeError:
            return HttpResponse("No valid json", status=400)
        if not form.is_valid():
            return JsonResponse(form.errors, status=400)

        username = form.cleaned_data['username']
        rating = form.cleaned_data['rating']

        self.create_or_update_rating(quote, username, rating)

        return self.get_json_response_for_quote_rating(quote)

    def get_json_response_for_quote_rating(self, quote_id):
        quotes = QuoteRating.objects.filter(quote_id=quote_id)

        return JsonResponse([
            self.serialize_rating(quote) for quote in quotes
        ], safe=False)  # safe to allow list to json encoding, only dicts are supported otherwise

    def create_or_update_rating(self, quote, username, rating):
        quote_rating, is_created = QuoteRating.objects.get_or_create(
            quote_id=quote,
            username=username,
            defaults={'rating': rating}
        )
        if not is_created:
            quote_rating.rating = rating
            quote_rating.save()

        return quote_rating

    def serialize_rating(self, rating):
        return {
            'username': rating.username,
            'rating': str(rating.rating)
        }
