from json import JSONDecodeError

from django.http import JsonResponse, HttpResponseNotFound
from django.views import View
from requests import HTTPError

from quotes_api.quote_service import get_quote_by_id
from quotes_api.rating import get_rate_information


class QuoteDetailView(View):
    def get(self, request, id, *args, **kwargs):
        try:
            quote_info = get_quote_by_id(id)
        except HTTPError:
            return JsonResponse({'message': 'Quotes API is unavailable'}, status=503)
        except (JSONDecodeError, KeyError):
            return JsonResponse({'message': 'Unexpected quotation data received'}, status=500)
        if quote_info is None:
            return HttpResponseNotFound()

        average_rating, number_of_rates = get_rate_information(id)
        return JsonResponse({
            'average_rating': str(average_rating) if average_rating else None,
            'number_of_rates': number_of_rates,
            **quote_info._asdict()
        })
