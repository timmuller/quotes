import re
from collections import namedtuple
import requests

Quote = namedtuple('Quote', ['id', 'author', 'quote', 'link'])


def get_quote_by_id(id):
    url = 'http://quotes.stormconsultancy.co.uk/quotes/{}.json'.format(id)
    ret = requests.get(url)
    ret.raise_for_status()
    return _response_to_quote(ret)


def get_random_quote():
    url = "http://quotes.stormconsultancy.co.uk/random.json"
    ret = requests.get(url)
    ret.raise_for_status()
    return _response_to_quote(ret)


def _response_to_quote(response):
    quote_data = response.json()
    return Quote(
        id=quote_data['id'],
        author=quote_data['author'],
        quote=convert_unicode_string(quote_data['quote']),
        link=quote_data['permalink'],
    )

def convert_unicode_string(data):
    encoded_data = data.encode('utf-8')
    matches = re.finditer(bytes(r'(\\x[0-9a-f]{2})', 'ascii'), encoded_data)
    for match in matches:
        start_index = match.start(0)
        end_index = match.end(0)

        print(match.start(0), match.end(0), match)
    return data
