import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    quote: {
      id: null,
      author: null,
      quote: null,
      link: null,
      average_rating: null,
      number_of_rates: 0,
    },
    quoteRates: null,
    apiAvailable: true,
    username: '',
  },
  mutations: {
    newQuote(state, quote) {
      state.quote = quote;
    },
    newRates(state, rates) {
      state.quoteRates = rates;
    },
    apiAvailable(state, available) {
      state.apiAvailable = available;
    },
    setUsername(state, username) {
      state.username = username;
    },
  },
  actions: {
    fetchRandomQuote({ commit }) {
      axios.get('api/random_quote/').then((res) => {
        commit('apiAvailable', true);
        commit('newQuote', res.data);
      }).catch(() => {
        commit('apiAvailable', false);
      });
    },
    getQuote({ commit }, quoteId) {
      axios.get(`api/quote/${quoteId}/`).then((res) => {
        commit('apiAvailable', true);
        commit('newQuote', res.data);
      }).catch(() => {
        commit('apiAvailable', false);
      });
    },
    setRate({ dispatch, commit, rootState }, rate) {
      const currentQuoteID = rootState.quote.id;
      const postData = {
        rating: rate,
        username: rootState.username,
      };
      return axios.post(`api/quote/${currentQuoteID}/rate/`, postData).then((res) => {
        dispatch('getQuote', currentQuoteID);
        commit('newRates', res.data);
      });
    },
    getQuoteRates({ commit }, quoteId) {
      axios.get(`api/quote/${quoteId}/rate/`).then((res) => {
        commit('apiAvailable', true);
        commit('newRates', res.data);
      }).catch(() => {
        commit('apiAvailable', false);
      });
    },
  },
  getters: {
    currentQuote(state) {
      return state.quote;
    },
    isApiAvailable(state) {
      return state.apiAvailable;
    },
    getUsername(state) {
      return state.username;
    },
    getQuoteRates(state) {
      return state.quoteRates;
    },
  },
});
